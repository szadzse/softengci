FROM node:8.10.0
COPY . .
EXPOSE 3000
CMD ["node", "app.js"]